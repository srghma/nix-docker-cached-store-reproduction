{ lib, ... }:

/*
  Example:
    recursiveUpdateList [ { a = "x"; c = "m"; } { a = "y"; b = "z"; } ]
    => { a = "y"; b = "z"; c="m"; }
*/

listOfAttrsets: lib.fold (attrset: acc: lib.recursiveUpdate attrset acc) {} listOfAttrsets
